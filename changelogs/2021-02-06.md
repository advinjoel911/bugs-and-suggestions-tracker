# Changelogs for Patch 1.2.5

This maintenance was mainly focused on delivering new event and fixes to WoE Treasure box.

# Event
## Valentine's day event is here!
Every year around mid-February people all around the world celebrate love by celebrating Saint Valentine's day, this year celebrate Valentine's by helping one of two people one in Prontera and Payon to celebrate with their loved ones!

Depending on which path you decide to take, the rewards will different. 

Rewards will be given once to each master account regardless of how many characters complete it.

To start the event check in with one of these NPCs:
* The old man `/navi payon 195/131`
* Andy `/navi prontera 146/237`

Once you finish the main quest you can start creating Valentine's Event Box  - which will have a chance to reward you with tons of consumables and exclusive Valentine's Day costumes!

You can find the NPC for this at:
* Ruby `/navi jawaii 124/194`

The Valentine's Day event box when opened can give:
* Common
	* 5x Strawberry
	* 5x Fresh Fish
	* 1x Awakening Potion
	* 1x Berserk Potion
* Uncommon
	* 1x World Tour Ticket
	* 1x [Event] Rapid Potion
	* 1x [Event] Challenge Drink
	* 1x [Event] Unlimited Drink
	* 1x [Event] Power Drink
	* 1x [Event] Mimir Spring Water
	* 1x Costume Lovely heart cap
	* 1x Costume Black warm cat muffler
	* 1x Costume Stardust hairband
* Rare
	* 1x Costume Frill collar
	* 1x Costume Sorin Doll hat
* Epic
	* 1x Costume Black vail
	* 1x Costume Black feather hat
	* 1x Enriched Elunium
	* 1x Enriched Oridecon
	* 1x Blacksmith's Blessing Shard
* Heroic
	* 1x Shining angel wings (Garment Costume)
	* Blacksmith's Blessing

# Cash shop
The girls selection, is our new costume set for cash shop available through mid-February, this new set is all about being kawaii while being reddish for the greatest Valentine’s Day ever. We also are introducing the first time limited Effect stone! Available during February, so make sure to get your hands on it while you can!

## Costume effect stone
Through February this new exclusive costume effect stone will be available! Romance Rose effect is a mid-headgear exclusive costume for an extra romantic touch!
* ![Romance Rose effect](https://i.gyazo.com/3d9aa00890651b3a1c8efb54a18626dc.gif)

## Girls selection
This set will be available until mid to late February (up until amatsu patch). This collection offers an amazing theme for valentines and it’s the first time we release a wig and two garment costumes back to back in one set!

* ![Red Bread Hat](https://i.gyazo.com/2cfd153fb6f57be581bebf66a13cfdcb.gif)
* ![Rainbow Wing](https://i.gyazo.com/7f3e0ef77bce2b3bde20ce3fb1fc0e15.gif)
* ![Shaman hat](https://i.gyazo.com/cc7d9e0560414dd75240e2e05f11a57c.gif)
* ![Lovely feeling](https://i.gyazo.com/57c39acef129e78f268d9616cb67fc0d.gif)
* ![Little Garden](https://i.gyazo.com/755d1acf7592f86eb10811f9a5dcb8b4.gif)
* ![White bird rose](https://i.gyazo.com/c22eafead9f612a330b8450566b30d09.gif)
* ![Lady Tanee](https://i.gyazo.com/1944f75ca3e8bbb5b25a01efc8d53ab0.gif)
* ![Sweet Cornet](https://i.gyazo.com/eb4d83c2901635946e61cabd73cc2ccc.gif)
* ![Big Ribben Cloak](https://i.gyazo.com/5c33571fc43ce9ab0ed908c7b15ffe32.gif)
* ![Chocolate Heart bag](https://i.gyazo.com/64ee38bcf3f32fb2c1935d6f9fbbe0f6.gif)

# Fixes
* Fixed some of the dropped Potions from treasure chest being account bound
* Corrected WoE costumes to be bound to account after wearing it for the first time, a warning box will be displayed to user before it happens.
