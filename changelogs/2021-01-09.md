# Changelogs for Patch 1.2.1

This is a hot patch to release the changes to WoE Treasure Chests (Boxes).

# WoE
* [Normal Treasure Chests](https://cp.playragnarokzero.com/woe/castles/) will spawn now. Godlike materials and other unrelated items to Zero have been removed. The WoE Supplies Box has been modified to supply a random amount of Guild Dungeon Coins.

# Fixes
* Cyber Income is now correctly worn on the upper headgear costume slot
