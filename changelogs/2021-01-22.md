# Changelogs for Patch 1.2.3

This is a hot patch to release the changes to PvP Arena and misc fixes

# Cash shop
* Fixed Costume: Scepter view
* Fixed Water Field effect
	* ![Water Field effect](https://i.gyazo.com/0a28c34d16e6051b11369b1545fe2059.gif)
	* Please note due to a problem in applying this patch you will need to relog the first time you equip the item, this will be fixed with the coming server restart on monday

# Project Zero: PvP Arena Season 0 Changes
Thank you everyone for your input! The following changes have been applied to Season 0!
* Added **PvP Potion Kitter NPC**: Moves `Potion Claim Kit` to from PvP Manager NPC to this NPC
* Increased **Potion Kit Claim amount** from `60/20 to 100/50`
* Added **PvP Rental Gear NPC**: Allows you to rent `Lesser Guild Gear` for 30 minutes at a time to let you easily hop into the action! (The renter is disabled on hour before WoE and until WoE ends)
* Reduced **Potion Kit** cost from 2 to 1 PvP Tokens.
* **Removed portals** from `guild_vs1` (WoE Damage Test Room) and `guild_vs3` (PvP Arena) maps
* **Changed color** for `#pvp` channel to be a bit brighter
* Upon entering the Preparation Room, **all buffs that can be dispelled via the Sage's Dispell skill will be removed**. (HQM buffs, etc. will stay active)

Please report any bugs and feedback to our [Season 0 GitLab](https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/-/issues/677) please!
* We still need some community suggestions for Season 1 rewards ;)

# WoE
We have delayed the release of the new TBs until next monday, the investment has been reset and so is castles, the TBs will be released on monday and no further reset will happen.

The weekend of 30th and 31st of January will also change WoE times to another proposal, Saturday WoE will be held at 10:00 AM server time, while Sunday WoE will be held at 1:00 AM server time, we hope this proposal would work out better for our player base.
